﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace S3nis_FTA
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class SIF : INotifyPropertyChanged
    {
        //SIF identification
        private string _sifCode;
        private string _sifName;
        private string _sifFunction;
        //SIF parameters
        private double _sifTi;    //Proof test interval (hours)
        private double _sifPTC;   //Proof test coverage (pecentage 0.0 - 1.0)
        private double _sifSD;    //Shutdown Time (hours)
        private double _sifMT;    //SIF Mission Time (years) //TODO: validar la unidad
        private double _sifBypassDuration;    //Permitted bypass duration (Hours)

        // SIF Requirement
        private double _sifReqFRR;    //Required Risk Reduction Factor
        private SIL _sifReqSIL;       //Required Safety Integrity Level
        private double _sifReqMTTFs;  //Required Mean Time To Fail safe.

        //SIF Configuration parameters
        private bool _sifOnlineRepair;        //SIF element can be repaired online?
        private OptionBeta _sifOptionBeta;    //Beta calculation style. (Minimun, Median, Maximun) 
        private AC_METHOD _sifUseAC;          // Architecture Contrains method to be used on calculations
        private bool _sifUseSC;               // Consider Sistematic Capability.
        private SIF_MODE _sifMode;            //Operation mode for de de SIF.
        private bool _energizedToTrip;        // Energized to trip. (1=normally de-energized)
        private bool _aaa;
            

        /// <summary>
        /// Class Contructor.
        /// </summary>
        public SIF() 
            : this("New SIF")
        {
        }

        /// <summary>
        /// Class Contructor.
        /// </summary>
        /// <param name="sifName"></param>
        public SIF(string sifName)
        {
            _sifName = sifName;
            _sifOptionBeta = OptionBeta.MED;    
            _sifMode = SIF_MODE.LOW_DEMAND;
            _sifUseAC = AC_METHOD.IEC61511;
            _sifUseSC = false;         //TODO: evaluar cambiar a true
            _sifOnlineRepair = false;  //TODO: evaluar cambiar a true
            _sifTi = 8760.0;
            _sifPTC = 1.0;
            _sifSD = 8.0;
            _sifMT = 1.0;                
            _sifReqSIL = SIL.NO_SIL;
            _sifReqFRR = 1.0;
        }


        #region SIF_public_parameters
        /// <summary>
        /// Proof test interval (hours)
        /// </summary>
        public string SifName
        {
            get { return _sifName; }
            set
            {
                if (_sifName != value)
                {
                    _sifName = value;
                    NotifyPropertyChanged("SifName");
                }
            }
        }

        /// <summary>
        /// SIF code
        /// </summary>
        public string SifCode
        {
            get { return _sifCode; }
            set
            {
                if (_sifCode != value)
                {
                    _sifCode = value;
                    NotifyPropertyChanged("SifCode");
                }
            }
        }

        /// <summary>
        /// SIF function description
        /// </summary>
        public string SifFunction
        {
            get { return _sifFunction; }
            set
            {
                if (_sifFunction != value)
                {
                    _sifFunction = value;
                    NotifyPropertyChanged("SifFunction");
                }
            }
        }
        
        /// <summary>
        /// SIF Proof test interval.
        /// </summary>
        public double SifTi
        {
            get { return _sifTi; }
            set
            {
                if (_sifTi != value)
                {
                    _sifTi = value;
                    NotifyPropertyChanged("SifTi");
                }
            }
        }

        /// <summary>
        /// SIF Proof test coverage
        /// </summary>
        public double SifPTC
        {
            get { return _sifPTC; }
            set
            {
                if (_sifPTC != value)
                {
                    _sifPTC = value;
                    NotifyPropertyChanged("SifPTC");
                }
            }
        }

        /// <summary>
        /// SIF Shutdown time (time for recovery after SIF activation)
        /// </summary>
        public double SifSD
        {
            get { return _sifSD; }
            set
            {
                if (_sifSD != value)
                {
                    _sifSD = value;
                    NotifyPropertyChanged("SifSD");
                }
            }
        }

        /// <summary>
        /// SIF Mission Time (Spected SIF lifetime)
        /// </summary>
        public double SifMT
        {
            get { return _sifMT; }
            set
            {
                if (_sifMT != value)
                {
                    _sifMT = value;
                    NotifyPropertyChanged("SifMT");
                }
            }
        }

        /// <summary>
        /// SIF Bypass Duration
        /// </summary>
        public double SifBypassDuration
        {
            get { return _sifBypassDuration; }
            set
            {
                if (_sifBypassDuration != value)
                {
                    _sifBypassDuration = value;
                    NotifyPropertyChanged("SifBypassDuration");
                }
            }
        }

        /// <summary>
        /// SIF minimun required Risk Reduction Factor.
        /// </summary>
        public double SifReqFRR
        {
            get { return _sifReqFRR; }
            set
            {
                if (_sifReqFRR != value)
                {
                    _sifReqFRR = value;
                    NotifyPropertyChanged("SifReqFRR");
                }
            }
        }

        /// <summary>
        /// SIF Requiered Safety Integrity Level.
        /// </summary>
        public SIL SifReqSIL
        {
            get { return _sifReqSIL; }
            set
            {
                if (_sifReqSIL != value)
                {
                    _sifReqSIL = value;
                    NotifyPropertyChanged("SifReqSIL");
                }
            }
        }

        /// <summary>
        /// Required Mean Time To Fail safe
        /// </summary>
        public double SifReqMTTFs
        {
            get { return _sifReqMTTFs; }
            set
            {
                if (_sifReqMTTFs != value)
                {
                    _sifReqMTTFs = value;
                    NotifyPropertyChanged("SifReqMTTFs");
                }
            }
        }

        /// <summary>
        /// SIF components can be repaired online.
        /// </summary>
        public bool SifOnlineRepair
        {
            get { return _sifOnlineRepair; }
            set
            {
                if (_sifOnlineRepair != value)
                {
                    _sifOnlineRepair = value;
                    NotifyPropertyChanged("SifOnlineRepair");
                }
            }
        }

        /// <summary>
        /// Indicated how beta factors will be used in model.
        /// MIN (-1): Minimun value
        /// MED ( 0): Average value
        /// MAX ( 1): Maximun value
        /// </summary>
        public OptionBeta SifOptionBeta
        {
            get { return _sifOptionBeta; }
            set
            {
                if (_sifOptionBeta != value)
                {
                    _sifOptionBeta = value;
                    NotifyPropertyChanged("SifOptionBeta");
                }
            }
        }
        /// <summary>
        /// Method to be used on Architecture constrains
        /// </summary>
        public AC_METHOD SifUseAC 
        {
            get { return _sifUseAC; }
            set
            {
                if (_sifUseAC != value)
                {
                    _sifUseAC = value;
                    NotifyPropertyChanged("SifUseAC");
                }
            }
        }

        /// <summary>
        /// Consider Sistematic Capability or not.
        /// </summary>
        public bool SifUseSC
        {
            get { return _sifUseSC; }
            set
            {
                if (_sifUseSC != value)
                {
                    _sifUseSC = value;
                    NotifyPropertyChanged("SifUseSC");
                }
            }
        }

        /// <summary>
        /// SIF mode: Low Demmand, High demand or continuos mode
        /// </summary>
        public SIF_MODE SifMode
        {
            get { return _sifMode; }
            set
            {
                if (_sifMode != value)
                {
                    _sifMode = value;
                    NotifyPropertyChanged("SifMode");
                }
            }
        }

        /// <summary>
        /// Energized to trip. (1=normally de-energized)
        /// </summary>
        public bool EnergizedToTrip
        {
            get { return _energizedToTrip; }
            set
            {
                if (_energizedToTrip != value)
                {
                    _energizedToTrip = value;
                    NotifyPropertyChanged("EnergizedToTrip");
                }
            }
        }
        //TODO: incluir grupos principales y subgrupos.
        //TODO: Incluir lista de documentos 

        #endregion

        #region SIF_Methods

        /// <summary>
        /// Override to an string for class SIF.
        /// </summary>
        public override string ToString()
        {
            return $"{SifName}";
        }
        //TODO: incluir metodos de calculo para la SIF
        #endregion


        #region SIF_Events
        /// <summary>
        /// PropertyChanged event 
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
        #endregion

    }



}
