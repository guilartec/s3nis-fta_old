﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.ObjectModel;

namespace S3nis_FTA
{
    public class Revision : INotifyPropertyChanged
    {
        private string _rev;
        private string _author;
        private DateTime _creationDate;
        private string _revisorName ;
        private DateTime _revisedDate;
        private string _approverName;
        private DateTime _approvalDate;
        private string _comments;

        public string Rev
        {
            get { return _rev; }
            set { _rev = value; }
        }


        public string Author
        {
            get { return _author; }
            set { _author = value; }
        }


        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public string RevisorName
        {
            get { return _revisorName; }
            set { _revisorName = value; }
        }


        public DateTime RevisedDate
        {
            get { return _revisedDate; }
            set { _revisedDate = value; }
        }


        public string ApproverName
        {
            get { return _approverName; }
            set { _approverName = value; }
        }



        public DateTime ApprovalDate
        {
            get { return _approvalDate; }
            set { _approvalDate = value; }
        }


        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
