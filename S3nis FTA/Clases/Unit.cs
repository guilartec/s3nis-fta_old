﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace S3nis_FTA
{
    /// <summary>
    /// Main Class for Unit. It represents -the plant's area where the SIF is located.
    /// </summary>
    [Serializable]
    public class Unit : INotifyPropertyChanged
    {
        private string _unitName;
        private string _unitDescription;
        /// <summary>
        /// Unit SIF Collection 
        /// </summary>
        public ObservableCollection<SIF> SIFS { get; set; }

        /// Constructor 
        public Unit() 
            :  this("New Unit")
        {            
        }
        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="unitName"></param>
        public Unit(string unitName)
        {
            UnitName = unitName;
            SIFS = new ObservableCollection<SIF>();
        }

#region Unit_Parameters
        /// <summary>
        /// Unit Name or code.
        /// </summary>
        public string UnitName  
        {
            get { return _unitName; } 
            set
            {
                if (_unitName != value)
                {
                    _unitName = value;
                    NotifyPropertyChanged("UnitName");
                }
            }
        }

        /// <summary>
        /// Unit Description.
        /// </summary>
        public string UnitDescription
        {
            get { return _unitDescription; }
            set
            {
                if (_unitDescription != value)
                {
                    _unitDescription = value;
                    NotifyPropertyChanged("UnitDescription");
                }
            }
        }
        #endregion

        /// <summary>
        /// Override to an string for class unit.
        /// </summary>
        public override string ToString()
        {
            return $"{UnitName}";
        }

        //TODO: incluir lista de elementos SIF

        /// <summary>
        /// PropertyChanged event 
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        
        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

    }
}
