﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace S3nis_FTA
{
    /// <summary>
    /// Group for elements for SIL determination
    /// </summary>
    [Serializable]
    public class Group : INotifyPropertyChanged
    {
        
        // Group Name
        private string name;
        // GroupDescription
        private string description;
        // Group Voting scheme
        private VOTING voting;
        private double pfdavg;
        private double frr;
        private    SIL sil;
        private double mttfs;
        private double pfs;
        private double str;
        private GROUPMODE mode;



        ///<summary>
        /// Class constructor
        /// </summary>
        /// /// <param name="Name">Name for the group</param>
        public Group(string Name)
        {
            name = Name;
            voting = VOTING._1oo1;
            mode = GROUPMODE.GROUP;
        }

        ///<summary>
        /// Class constructor
        /// </summary>
        public Group()
            : this("New Group")
        {
        }

        /// <summary>
        /// Name for the Group
        /// </summary>
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Description for the Group
        /// </summary>
        public string Description
        {
            get { return description; }
            set
            {
                if (description != value)
                {
                    description = value;
                    OnPropertyChanged(); 
                }
            }
        }

        /// <summary>
        /// Group Voting scheme
        /// </summary>
        public VOTING Voting
        {
            get { return voting; }
            set
            {
                if (voting != value)
                {
                    voting = value;
                    OnPropertyChanged(); 
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public GROUPMODE Mode 
        {
            get { return mode; }
            set
            {
                if (mode != value)
                {
                    mode = value;
                    OnPropertyChanged();
                }
            }
            
        }


        #region Event Methods

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Notifies listeners that a property value has changed.
        /// </summary>
        /// <param name="propertyName">
        ///     Name of the property used to notify listeners.  This
        ///     value is optional and can be provided automatically when invoked from compilers
        ///     that support <see cref="CallerMemberNameAttribute" />.
        /// </param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }

}