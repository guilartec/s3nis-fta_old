﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace S3nis_FTA
{
    /// <summary>
    /// Represents documents used as reference for the study, such as drawings, norms, 
    /// </summary>
    [Serializable]
    public class Doc :  INotifyPropertyChanged
    {
        private string code;
        private string name;
        private string description;
        private string author;
        private string revision;
        private DateTime revisionDate;
        private string url;
        private string category;
        private string resumen;
        private string path;

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="code"></param>
        public Doc(string code)
        {
            this.code = code;
        }


        #region Properties
        /// <summary>
        /// Document's Code.
        /// </summary>
        public string Code
        {
            get { return code; }
            set
            {
                if (code != value)
                {
                    code = value;
                    OnPropertyChanged();
                }
            }
        }
        /// <summary>
        /// Document's Name
        /// </summary>
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged();
                }
            }
        }
        /// <summary>
        /// Document Description
        /// </summary>
        public string Description
        {
            get { return description; }
            set
            {
                if (description != value)
                {
                    description = value;
                    OnPropertyChanged();
                }

            }
        }
        /// <summary>
        /// Document Author
        /// </summary>
        public string Author
        {
            get { return author; }
            set
            {
                if (author != value)
                {
                        author = value;
                    OnPropertyChanged();
                }

            }
        }
        /// <summary>
        /// Document Revision code. (A, B, C, 0, etc)
        /// </summary>
        public string Revision
        {
            get { return revision; }
            set
            {
                if (revision != value)
                {
                    revision= value;
                    OnPropertyChanged();
                }

            }
        }
        /// <summary>
        /// Document Date's of last Revision 
        /// </summary>
        public DateTime RevisionDate
        {
            get { return revisionDate; }
            set
            {
                if (revisionDate != value)
                {
                    revisionDate = value;
                    OnPropertyChanged();
                }

            }
        }

        public string Url
        {
            get { return url; }
            set
            {
                if (url != value)
                {
                    url = value;
                    OnPropertyChanged();
                }

            }
        }
        public string Category
        {
            get { return category; }
            set
            {
                if (category != value)
                {
                    category = value;
                    OnPropertyChanged();
                }

            }
        }
        public string Resumen
        {
            get { return resumen; }
            set
            {
                if (resumen != value)
                {
                    resumen = value;
                    OnPropertyChanged();
                }

            }
        }

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;


        #endregion
        #region Event Methods

        /// <summary>
        ///     Notifies listeners that a property value has changed.
        /// </summary>
        /// <param name="propertyName">
        ///     Name of the property used to notify listeners.  This
        ///     value is optional and can be provided automatically when invoked from compilers
        ///     that support <see cref="CallerMemberNameAttribute" />.
        /// </param>
        [field: NonSerialized]
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Documents : ObservableCollection<Doc>
    {
        private ObservableCollection<Doc> documentos;
        private ObservableCollection<string> categories;


        //public ObservableCollection<Doc> Documentos 
        //{ 
        //    get => documentos; 
        //    set => documentos = value; 
        
        //}
        //public ObservableCollection<string> Categories { get => categories; set => categories = value; }


        //#region Events

        ///// <summary>
        ///// 
        ///// </summary>
        //[field: NonSerialized]
        //public event PropertyChangedEventHandler PropertyChanged;
        ///// <summary>
        ///// 
        ///// </summary>
        //[field: NonSerialized] 
        //public event NotifyCollectionChangedEventHandler CollectionChanged;


        //#endregion
        //#region Event Methods

        ///// <summary>
        /////     Notifies listeners that a property value has changed.
        ///// </summary>
        ///// <param name="propertyName">
        /////     Name of the property used to notify listeners.  This
        /////     value is optional and can be provided automatically when invoked from compilers
        /////     that support <see cref="CallerMemberNameAttribute" />.
        ///// </param>
        
        //protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    NotifyCollectionChangedEventHandler eventHandler = this.CollectionChanged;
        //    if (eventHandler != null)
        //    {
        //        eventHandler(this, new NotifyCollectionChangedEventHandler(CollectionChanged));
        //    }
        //}
        ///#endregion
    }



}
