﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace S3nis_FTA
{
    /// <summary>
    /// Indicate how beta factors will be used in model.
    /// </summary>
    public enum OptionBeta
    {

        /// <summary>
        /// Use lowest value.
        /// </summary>
        [Description("Minimum Value")] MIN = -1,
        /// <summary>
        /// Use average value.
        /// </summary>
        [Description("Average Value")] MED = 0,
        /// <summary>
        /// Use highest value.
        /// </summary>
        [Description("Maximum Value")] MAX = 1
    }

    /// <summary>
    /// Define allowed values for SIL
    /// </summary>
    public enum SIL
    {
        /// <summary>
        /// Safety Integrity Not Required
        /// </summary>
        [Description("NO SIL")] NO_SIL = 0,
        /// <summary>
        /// Safety Integrity Required ( 10 <= FRR < 100)
        /// </summary>
        [Description("SIL 1")] SIL_1 = 1,
        /// <summary>
        /// Safety Integrity Required ( 100 <= FRR < 1000)
        /// </summary>
        [Description("SIL 2")] SIL_2 = 2,
        /// <summary>
        /// Safety Integrity Required ( 1000 <= FRR < 10000)
        /// </summary>
        [Description("SIL 3")] SIL_3 = 3,
        /// <summary>
        /// Safety Integrity Required ( FRR >= 100)
        /// </summary>
        [Description("SIL 4")] SIL_4 = 4

    }

    /// <summary>
    /// Define method used for architecture constrains
    /// </summary>
    public enum AC_METHOD
    {
        /// <summary>
        /// Architecture constrains no considered on calculations
        /// </summary>        
        [Description("No considered")] NOT_USED = 0,
        /// <summary>
        /// Architecture constrains no considered on calculations
        /// </summary>        
        [Description("Use IEC 61508 Method")] IEC61508 = 1,
        /// <summary>
        /// Architecture constrains no considered on calculations
        /// </summary>        
        [Description("Use IEC 61508 Method")] IEC61511 = 2

    }

    /// <summary>
    /// Mode for the SIF to operate
    /// </summary>
    public enum SIF_MODE
    {
        /// <summary>
        /// Low Demand Mode (on denand, Less than one demand per year)
        /// </summary>
        LOW_DEMAND = 0,
        /// <summary>
        /// High Denamd Mode (on denand, More than one demand per year)
        /// </summary>
        HIGH_DEMAND = 1,
        /// <summary>
        /// Continuous Mode (continuous operation)
        /// </summary>
        CONTINUOUS = 2
    }

    /// <summary>
    /// Voting schemes
    /// </summary>
    public enum VOTING
    {
        [Description("1oo1")] _1oo1,
        [Description("1oo2")] _1oo2,
        [Description("2oo2")] _2oo2,
        [Description("1oo3")] _1oo3,
        [Description("2oo3")] _2oo3,
        [Description("3oo3")] _3oo3,
        [Description("1oo4")] _1oo4,
        [Description("2oo4")] _2oo4,
        [Description("3oo4")] _3oo4,
        [Description("4oo4")] _4oo4,
        [Description("MooN")] _MooN


    }

    /// <summary>
    /// 
    /// </summary>
    public enum GROUPMODE
    {
        [Description("NODE")] NODE = 0,
        [Description("GROUP")] GROUP,
        [Description("DIRECT")] DIRECT
    }


}