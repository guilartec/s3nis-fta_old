﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace S3nis_FTA
{
    class DoublePercentageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter,
                        System.Globalization.CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                                    System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(double) || value == null)
                return value;

            string str = value.ToString();

            if (String.IsNullOrWhiteSpace(str))
                return 0.0f;
            string temp = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;

            str = str.TrimEnd(culture.NumberFormat.PercentSymbol.ToCharArray());
            //str = str.Replace('.', ',');
            //double.TryParse(str,Number, )
            double result = 0.0f;
            if ( double.TryParse(str, NumberStyles.Number, culture, out result) )
            {
                result /= 100.0;
            }

            return result;

        }
    }
}
