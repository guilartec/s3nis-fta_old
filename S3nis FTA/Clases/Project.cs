﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Windows;
using S3nis_FTA.Paginas;

namespace S3nis_FTA 
{
    /// <summary>
    /// Class representing Project Information.
    /// </summary>
    [Serializable]
    public class Project : INotifyPropertyChanged
    {
        private string _projectName;
        private string _companyName;
        private string _location;
        private string _revision;
        private string _author;
        private DateTime _modificationDate;
        private static DateTime _creationDate;
        private System.Version _s3nisVersion;
        private int _hasChanges;
        private ObservableCollection<Unit> _units;
        private Documents docs;
        

        //Constructor 
        public Project()
            : this("New Project")
        {
        }

        //Main Constructor 
        public Project(string projectName)
        {
            _projectName = projectName;
            _creationDate = DateTime.Now;
            _modificationDate = _creationDate;
            _s3nisVersion = Assembly.GetEntryAssembly().GetName().Version;
            units = new ObservableCollection<Unit>();
            Docs = new Documents();
            //DocCategories = new ObservableCollection<string>() { "Manual de seguridad", "DTI", "Plano", "Lógica", "Procedimiento", "Otro" };
        }

        //Project Name property
        public string ProjectName
        {
            get { return _projectName; }
            set
            {
                if (_projectName != value)
                {
                    _projectName = value;
                    NotifyPropertyChanged("ProjectName");
                }
            }
        }

        //Company Name property
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                if (_companyName != value)
                {
                    _companyName = value;
                    NotifyPropertyChanged("CompanyName");
                }
            }
        }

        //Location of the Company
        public string Location
        {
            get { return _location; }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    NotifyPropertyChanged("Location");
                }
            }
        }

        //Revision of the Project
        public string Revision
        {
            get { return _revision; }
            set
            {
                if (_revision != value)
                {
                    _revision = value;
                    NotifyPropertyChanged("Revision");
                }
            }
        }

        //Author of the Project.
        public string Author
        {
            get { return _author; }
            set
            {
                if (_author != value)
                {
                    _author = value;
                    NotifyPropertyChanged("Author");
                }
            }
        }

        //Project's Creation date
        public static DateTime CreationDate { get => _creationDate; }

        //S3nis FTA Version
        public string S3nisVersion
        {
            get => string.Format("Version {0}.{1}",
                _s3nisVersion.Major, _s3nisVersion.Minor);
            //this property is set once on project creation.
        }

        //Project's last modification date. It is set on project saving events.
        public DateTime ModificationDate
        {
            get { return _modificationDate; }
            set
            {
                if (_modificationDate != value)
                {
                    _modificationDate = value;
                    NotifyPropertyChanged("ModificationDate");
                }
            }
        }

        public ObservableCollection<Unit> units
        {
            get => _units;
            set => _units = value;
        }
        public Documents Docs { get => docs; set => docs = value; }






        //public ObservableCollection<Unit> Units { get => units; set => units = value; }



        //TODO: Incluir coleccion de documentos
        //TODO: Incluir coleccion de componentes para ser reusados
        //TODO: Incluir 

        //TODO: Incluir metodos para las operaciones del proyecto: crear, abrir, guardar, guardar como. Evaluar si van en la clase...

        /// <summary>
        /// Override to an string for the main project.
        /// </summary>
        public override string ToString()
        {
            return $"{ProjectName}";
        }

        #region INotifyPropertyChanged Members
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        [field: NonSerialized]
        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

        #endregion

    }


}
