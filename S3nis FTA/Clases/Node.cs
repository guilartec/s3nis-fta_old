﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace S3nis_FTA
{
    /// <summary>
    /// 
    /// </summary>
    public class Node : INotifyPropertyChanged
    {
        private string nodeName;            //Node Name
        private string nodeDescription;     //Node Description
        private string nodeVoting;          //Node Voting scheme (string to be converted)
        private readonly int _M;                     //internal. Number of element to fail in voting scheme MooN
        private readonly int _N;                     //internal. Number of element voting 

        private ObservableCollection<Group> _components;


        /// <summary>
        ///     
        /// </summary>
        public  string NodeName
        {
            get { return nodeName; }
            set
            {
                if (nodeName != value)
                {
                    nodeName = value;
                    NotifyPropertyChanged("NodeName");
                }
            }
        }

        /// <summary>
        ///  
        ///</summary>
        public string NodeDescription
        {
            get { return nodeDescription; }
            set
            {
                if (nodeDescription != value)
                {
                    nodeDescription = value;
                    NotifyPropertyChanged("NodeDescription");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string NodeVoting
        {
            get { return nodeVoting; }
            set
            {
                if (nodeVoting != value)
                {
                    nodeVoting = value;
                    //TODO: validar el dato "#oo#"
                    NotifyPropertyChanged("NodeVoting");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Group> Components
        {
            get { return _components; }
            set { _components = value; }
        }




        private void validate_voting(string voting)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 
        /// </summary>
        private void NotifyPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }

    }
}
