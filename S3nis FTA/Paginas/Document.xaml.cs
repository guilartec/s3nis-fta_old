﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace S3nis_FTA.Paginas
{
    /// <summary>
    /// Lógica de interacción para GroupSensor.xaml
    /// </summary>
    public partial class Document : Page
    {
        public Document()
        {
            InitializeComponent();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Add Documento to project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog document = new OpenFileDialog();
            document.Filter = "S3NIS FTA (*.S3FTA)|*.s3fta|All files (*.*)|*.*";
            document.Title = "Seleccione un Proyecto";
            if (document.ShowDialog() == true)
            {
                String filePath = document.FileName;
                var fileStream = document.OpenFile();
                MessageBox.Show("El Documento se incluirá a la lista maestra del proyecto", "Nuevo Documento", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
    
}
