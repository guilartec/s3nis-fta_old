﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using S3nis_FTA;
namespace S3nis_FTA.Paginas
{
    /// <summary>
    /// Lógica de interacción para pageSIF.xaml
    /// </summary>
    public partial class pageSIF : Page
    {
        Group CurrentGroup;
        public pageSIF()
        {
            CurrentGroup = new Group()
            {
                Name = "Horno F1001",
                Description = "Ejemplo Grupo",
                Voting = VOTING._1oo2
            };
            InitializeComponent();
            //var groupsensor = new GroupSensor();
            this.DataContext = CurrentGroup;
            //GroupPrincipal.NavigationService.Navigate(new GroupSensor());
        }

        private void DataGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Component());
        }

        #region show option Arquitecture AC
        /// <summary>
        /// Option Arquitecture AC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void AC_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (AC.IsChecked != true) { ACArq.IsEnabled = true; }
        //    else 
        //    {
        //        ACArq.SelectedItem =null;
        //        ACArq.IsEnabled = false; 
        //    }
        //}
        #endregion

        #region button Previous and Next
        /// <summary>
        /// Move forward on the main Tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            TabPrincipal.SelectedIndex += 1;
        }

        /// <summary>
        /// Move back on the main Tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPrevious_Click(object sender, RoutedEventArgs e)
        {
            TabPrincipal.SelectedIndex -= 1;
        }
        #endregion

        #region show button after and before in TabPrincipal
        private void TabPrincipal_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (TabPrincipal.SelectedIndex)
            {
                case 0:
                    BtnPrevious.Visibility = Visibility.Hidden;
                    BtnNext.Visibility = Visibility.Visible;
                    break;
                case 5:
                    BtnPrevious.Visibility = Visibility.Visible;
                    BtnNext.Visibility = Visibility.Hidden;
                    break;
                default:
                    BtnPrevious.Visibility = Visibility.Visible;
                    BtnNext.Visibility = Visibility.Visible;
                    break;
            }
        }
        #endregion

        private void NodoSif_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //TODO Verificar llamada al metodo.
            Page groupselect = new Page();

            if (NodoSif.Items.IndexOf(NodoSif.SelectedItem) == 0)
            {
                groupselect = new GroupSensor() {DataContext = NodoSif.SelectedItem};
            }
            else if (NodoSif.Items.IndexOf(NodoSif.SelectedItem) == 1)
            {
                groupselect = new GroupControl() { DataContext = NodoSif.SelectedItem };
            }
            else if (NodoSif.Items.IndexOf(NodoSif.SelectedItem) == 2)
            {
                groupselect = new GroupEFinal() { DataContext = NodoSif.SelectedItem };
            }
            else if(NodoSif.Items.IndexOf(NodoSif.SelectedItem) == -1)
            {
                groupselect = new NodoSensor() { DataContext = NodoSif.SelectedItem };
            }
            GroupPrincipal.NavigationService.Navigate(groupselect);
            TabPrincipal.SelectedIndex=1;
        }
    }
}
    
