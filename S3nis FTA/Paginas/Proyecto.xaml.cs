﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace S3nis_FTA.Paginas
{
    /// <summary>
    /// Lógica de interacción para Proyecto.xaml
    /// </summary>
    public partial class Proyecto : Page
    {
        public Proyecto()

        {
            InitializeComponent();
            Project CurrentProject = new Project();
            DataContext = CurrentProject;
        }

        private void Proyecto1_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void BtnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Unidades());
        }

        private void AddUnit_Click(object sender, RoutedEventArgs e)
        {
            dataUnit.CanUserAddRows = true;

        }
    }
}
