﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace S3nis_FTA.Paginas
{
    /// <summary>
    /// Lógica de interacción para Unidades.xaml
    /// </summary>
    public partial class Unidades : Page
    {
        public Unidades()
        {
            InitializeComponent();
            Unit CurrentUnit = new Unit();
            DataContext = CurrentUnit;
        }

        private void BtnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new pageSIF());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
