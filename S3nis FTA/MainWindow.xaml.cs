﻿#region using Declaration
using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Ribbon;
using Microsoft.Win32;
using System.Runtime.Remoting.Channels;
using System.Diagnostics;
using S3nis_FTA.Paginas;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.CompilerServices;
#endregion
namespace S3nis_FTA
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        Project CurrentProject;
        ObservableCollection<string> DocumentCategories ;
        
        /// <summary>
        /// Main window for the program.
        /// </summary>
        public MainWindow()
        {
            CurrentProject = new Project()
            {
                ProjectName = "Barrancabermeja",
                Author = "Carlos Guilarte",
                CompanyName = "CSF Consultoría en Seguridad Funcional",
                units = new ObservableCollection<Unit>()
                {
                    new Unit()
                    {
                        UnitName = "UNIDAD 100",
                        UnitDescription = "Unidad de Destilacion",
                        SIFS = new ObservableCollection<SIF>()
                        {
                            new SIF()
                            {
                                SifCode = "SIF-001",
                                SifName = "Proteccion por alto nivel del T-001",
                                SifFunction = "Al detectarse un nivel por elcima de 85% en el T-001 debe cerrar la valvula UV-001",
                                SifReqSIL = SIL.SIL_1,
                                SifReqFRR = 15,
                                SifTi = 17800,
                                SifMT = 10,
                                SifBypassDuration = 8,
                                SifPTC = 0.95,
                                SifSD = 12,
                                SifOnlineRepair = false,
                                SifOptionBeta = OptionBeta.MED
                            },
                            new SIF()
                            {
                                SifCode = "SIF-002",
                                SifName = "Proteccion por bajo nivel del T-001",
                                SifReqSIL = SIL.SIL_1,
                                SifReqFRR = 25,
                                SifTi = 17800,
                                SifMT = 10,
                                SifBypassDuration = 8,
                                SifPTC = 0.95,
                                SifSD = 12,
                                SifOnlineRepair = false,
                                SifOptionBeta = OptionBeta.MED
                            }
                        }

                    },
                    new Unit()
                    {
                        UnitName = "UNIDAD 300",
                        UnitDescription = "Unidad de tratamientod de aguas",
                        SIFS = new ObservableCollection<SIF>()
                        {
                            new SIF()
                            {
                                SifCode = "SIF-301",
                                SifName = "Proteccion por alta presion compresor K-01",
                                SifReqSIL = SIL.SIL_1,
                                SifReqFRR = 15,
                                SifTi = 17800,
                                SifMT = 10,
                                SifBypassDuration = 8,
                                SifPTC = 0.95,
                                SifSD = 12,
                                SifOnlineRepair = false,
                                SifOptionBeta = OptionBeta.MED
                            },
                            new SIF()
                            {
                                SifCode = "SIF-302",
                                SifName = "Proteccion alta temperatura K-01",
                                SifReqSIL = SIL.SIL_1,
                                SifReqFRR = 25,
                                SifTi = 17800,
                                SifMT = 10,
                                SifBypassDuration = 8,
                                SifPTC = 0.95,
                                SifSD = 12,
                                SifOnlineRepair = false,
                                SifOptionBeta = OptionBeta.MED
                            }
                        }
                    }
                },
                Docs = new Documents()
                {
                    new Doc("Documento1"),
                    new Doc("Documento2"),
                    new Doc("Documento3"),
                    new Doc("Documento4")
                }

            
            };

            ///DocumentCategories = CurrentProject.DocCategories;
            
            InitializeComponent();
            Inicio inicio = new Inicio();
            Principal.NavigationService.Navigate(inicio);
    
            this.DataContext = CurrentProject;
            ArbolPrincipal.DataContext = CurrentProject.units;
            
        }

       
        // TODO: incluir proposito y documentacion de la funcion
        private void RibbonApplicationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        // TODO: incluir proposito y documentacion de la funcion
        private void OpenProject_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog Archivo = new OpenFileDialog();
            Archivo.Filter = "S3NIS FTA (*.S3FTA)|*.s3fta|All files (*.*)|*.*";
            Archivo.Title = "Seleccione un Proyecto";
            if (Archivo.ShowDialog() == true)
            {
                String filePath = Archivo.FileName;
                //var fileStream = Archivo.OpenFile();
                FileStream file = new FileStream(filePath, FileMode.Open);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                CurrentProject= (Project)binaryFormatter.Deserialize(file);
                file.Close();

                Proyecto prj = new Proyecto();
                prj.DataContext = CurrentProject;
                Principal.NavigationService.Navigate(prj);

                ArbolPrincipal.DataContext = CurrentProject.units;
                TitleProject.Text = CurrentProject.ProjectName;
                MessageBox.Show("Proyecto cargado satisfactoriamente", "Cargar Proyecto");

            }
        }
        /// <summary>
        /// Upload the project to the main page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TitleProject_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Proyecto prj = new Proyecto();
            prj.DataContext = CurrentProject;
            Principal.NavigationService.Navigate(prj);
            MenuPrincipal.SelectedIndex = 0;
        }

        // TODO: incluir proposito y documentacion de la funcion
        private void TreeUnidades_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Unidades und = new Unidades();
            try
            {
                        und.DataContext = CurrentProject.units;
                        Principal.NavigationService.Navigate(und); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error ",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            Principal.NavigationService.Navigate(new Unidades());
            MenuPrincipal.SelectedIndex = 0;
            GrupoSif.Visibility = Visibility.Collapsed;
            GrupoUnidad.Visibility = Visibility.Visible;
        }

        // TODO: incluir proposito y documentacion de la funcion
        private void TreeSis_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Principal.NavigationService.Navigate(new pageSIF());
            MenuPrincipal.SelectedIndex = 0;
            GrupoSif.Visibility = Visibility.Visible;
            GrupoUnidad.Visibility = Visibility.Collapsed;
        }

        // TODO: incluir proposito y documentacion de la funcion
        private void Salir_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Hide / Show Side Panel 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HideLateral_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var col = new ColumnDefinition();
            Image imagen = new Image();
            BitmapImage im = new BitmapImage();
            col.Width = new System.Windows.GridLength(30);

            ImageBrush image = new ImageBrush();
            image.ImageSource = im;

            if (SidePanel.ActualWidth != 25)
            {
                col.Width = new System.Windows.GridLength(25);
                im.BeginInit();
                im.UriSource = new System.Uri("pack://application:,,,/Recursos/expand_32.png");
                im.EndInit();
                imagen.Source = im;
                PanelDocument.Visibility = Visibility.Collapsed;
                TitleProject.Visibility = Visibility.Collapsed;
                ArbolPrincipal.Visibility = Visibility.Collapsed;
            }
            else
            {
                col.Width = new System.Windows.GridLength(200);
                im.BeginInit();
                im.UriSource = new System.Uri("pack://application:,,,/Recursos/colapse_32.png");
                im.EndInit();
                imagen.Source = im;
                PanelDocument.Visibility = Visibility.Visible;
                TitleProject.Visibility = Visibility.Visible;
                ArbolPrincipal.Visibility = Visibility.Visible;
            }
            Lateral.Width = col.Width;
        }

        #region Select TreeView in  panel lateral
        /// <summary>
        /// Process TreeView element selection to show proper detail page. 
        /// and shows GroupSIF or GroupUnit in RibbonTab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ArbolPrincipal_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (ArbolPrincipal.SelectedItem is Unit)
            {
                Principal.NavigationService.Navigate(new Unidades() { DataContext = ArbolPrincipal.SelectedItem });
                GrupoSif.Visibility = Visibility.Collapsed;
                GrupoUnidad.Visibility = Visibility.Visible;
            }
            else if (ArbolPrincipal.SelectedItem is SIF)
            {
                Principal.NavigationService.Navigate(new pageSIF() { DataContext = ArbolPrincipal.SelectedItem});
                GrupoSif.Visibility = Visibility.Visible;
                GrupoUnidad.Visibility = Visibility.Collapsed;
            };
        }
        #endregion

        #region Exit program from Ribbon Menu
        /// <summary>
        /// Exit the program and save changes if necessary from Ribbon Menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Exit program from buttom exit from windows principal
        /// <summary>
        /// Exit the program and save changes if necessary from button EXIT the principal windows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var resp = MessageBox.Show("Desea Salir del programa S3NIS FTA (Si/No)", "Salir", MessageBoxButton.YesNo, MessageBoxImage.Question,MessageBoxResult.No);
            if (resp == MessageBoxResult.Yes)
            {
                //TODO: codigo para guardar el proyecto  antes de salir.
                Application.Current.Shutdown();
            }
            else
            {
                e.Cancel=true;
            }
        }

        private void AgregarUnidad_MouseDown(object sender, MouseButtonEventArgs e)
        {
            CurrentProject.units.Add(new Unit());
        }

        private void EliminarUnidad_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(ArbolPrincipal.SelectedItem is Unit)
            {
               if( MessageBox.Show( "¿Esta seguro de eliminar la Unidad seleccionada? (" 
                   + ArbolPrincipal.SelectedItem.ToString()+")") == MessageBoxResult.Yes )
                {
                    CurrentProject.units.Remove(((Unit)ArbolPrincipal.SelectedItem));
                }
            }
        }
        #endregion

        private void NewProject_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Verificar si el proyecto actual ha sido editado (variable en proyecto)
            //CurrentProject.
            //Si ha sido editada crear mensaje de error y preguntar si desea guardar.
            //Si desea guardar llamar a la rutina de guardar proyecto
            //Si no, destruir el objeto current_project, dispose
            //crear dialogo  para abrir solicitar la ubicacion del archivo de proyecto. (se creara una carpeta)
            //Validar la ubicacion y el nombre del archivo ingresado.
            //Si la seleccion de la carpeta base y el nombre son correctos 
            //crear una carpeta con el nombre del proyecto,
            //Dentro de la carpeta crear una carpeta adicional para los documentos y una para los reportes impresos.
            //Instanciar un nuevo objeto proyecto con el nombre indicado y guardar una copia serializada del objeto inicial en la carpeta
            // el nombre del archivo del proyecto tendra la extension .s3fta.
            //Asignar el objeto proyecto al datacontext del arbos lateral y cargar la ppagina de proyecto para editar los datos.

            CurrentProject = new Project(null);
            
            this.DataContext = CurrentProject;
            ArbolPrincipal.DataContext = CurrentProject;
            TitleProject.Text = CurrentProject.ProjectName;
            Principal.NavigationService.Navigate(new Proyecto() { DataContext = CurrentProject });
            
            return;
            
            OpenFileDialog Archivo = new OpenFileDialog();
            Archivo.Filter = "S3NIS FTA (*.S3FTA)|*.s3fta|All files (*.*)|*.*";
            Archivo.Title = "Seleccione un Proyecto";
            if (Archivo.ShowDialog() == true)
            {
                String filePath = Archivo.FileName;
                var fileStream = Archivo.OpenFile();
            }
        }

        private void AddDocument_Click(object sender, RoutedEventArgs e)
        {

            Principal.NavigationService.Navigate(new Document() { DataContext = CurrentProject.Docs }) ;
            
        }

        private void SaveProject_Click(object sender, RoutedEventArgs e)
        {
            string filePath = null;
            SaveFileDialog file = new SaveFileDialog();
            file.Filter = "S3NIS FTA (*.S3FTA)|*.s3fta|All files (*.*)|*.*";
            file.Title = "Seleccione un Proyecto";
            if (file.ShowDialog() == true)
            {
                filePath = file.FileName;
                //var fileStream = file.OpenFile();
                FileStream archivo = new FileStream(filePath, FileMode.Create);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(archivo, CurrentProject);
                archivo.Close();
                MessageBox.Show("Projecto guardado satisfactoriamente", "Guardar projecto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }


    }
}